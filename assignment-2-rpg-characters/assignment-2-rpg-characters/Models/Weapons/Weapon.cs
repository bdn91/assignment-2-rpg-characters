﻿using assignment_2_rpg_characters.Enums;
using assignment_2_rpg_characters.Interfaces;

namespace assignment_2_rpg_characters.Models.Weapons
{
    public class Weapon : Slot, IWeapon
    {
        public string Name { get; set; }
        public WeaponType WeaponType { get; set; }
        public int Damage { get; set; }
        public double Scale { get; set; }

        public Weapon()
        {
            SlotType = SlotType.Weapon;
        }

        public override string ToString()
        {
            return $"Items stats for: {Name}\n" +
                    $"Weapon Type: {WeaponType}\n" +
                    $"Weapon Level: {RequiredLevel}\n" +
                    $"Damage: {Damage}\n";
        }
    }
}
