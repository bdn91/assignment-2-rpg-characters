﻿using assignment_2_rpg_characters.Interfaces;
using assignment_2_rpg_characters.Enums;

namespace assignment_2_rpg_characters.Models.Weapons
{
    public class MeleeWeapon : Weapon
    {
        public MeleeWeapon(string name, int requiredLevel = 1)
        {
            Name = name;
            WeaponType = WeaponType.Melee;
            //Weapons damage without hero scaling
            Damage = 15 + (2 * requiredLevel);
            Scale = 1.5;
            RequiredLevel = requiredLevel;
        }
    }
}