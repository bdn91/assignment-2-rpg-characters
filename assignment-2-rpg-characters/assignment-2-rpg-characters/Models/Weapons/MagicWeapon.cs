﻿using assignment_2_rpg_characters.Enums;
using assignment_2_rpg_characters.Interfaces;

namespace assignment_2_rpg_characters.Models.Weapons
{
    public class MagicWeapon : Weapon
    {
        public MagicWeapon(string name, int minimumLevel = 1)
        {
            Name = name;
            WeaponType = WeaponType.Magic;
            //Weapons damage without hero scaling
            Damage = 25 + (2 * minimumLevel);
            Scale = 3;
            RequiredLevel = minimumLevel;
        }
    }
}
