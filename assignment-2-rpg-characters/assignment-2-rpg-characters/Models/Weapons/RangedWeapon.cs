﻿using assignment_2_rpg_characters.Interfaces;
using assignment_2_rpg_characters.Enums;


namespace assignment_2_rpg_characters.Models.Weapons
{
    public class RangedWeapon : Weapon
    {
        public RangedWeapon(string name, int requiredLevel = 1)
        {
            Name = name;
            WeaponType = WeaponType.Ranged;
            //Weapons damage without hero scaling
            Damage = 5 + (3 * RequiredLevel);
            Scale = 2;
            RequiredLevel = requiredLevel;
        }
    }
}
