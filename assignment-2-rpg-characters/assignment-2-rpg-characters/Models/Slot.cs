﻿using assignment_2_rpg_characters.Enums;
using assignment_2_rpg_characters.Interfaces;
using assignment_2_rpg_characters.Models.Stats;

namespace assignment_2_rpg_characters.Models
{
    public class Slot : ISlot
    {
        public SlotType SlotType { get; set; }
        public int RequiredLevel { get; set; }
        public IStat Bonus { get; set; } = new Stat();
    }
}
