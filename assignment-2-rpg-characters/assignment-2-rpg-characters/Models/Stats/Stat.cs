﻿using assignment_2_rpg_characters.Interfaces;
namespace assignment_2_rpg_characters.Models.Stats
{
    public class Stat : IStat
    {
        public int Health { get; set; }
        public int Strength { get; set; }
        public int Dexterity { get; set; }
        public int Intelligence { get; set; }
    }
}
