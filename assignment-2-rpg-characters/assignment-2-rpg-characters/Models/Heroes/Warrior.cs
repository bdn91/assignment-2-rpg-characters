﻿using assignment_2_rpg_characters.Models.Stats;

namespace assignment_2_rpg_characters.Models.Heroes
{
    public class Warrior : Hero
    {
        //level can be sent as a value for easier debugging and testing
        public Warrior(string name, int level = 1)
        {
            Name = name;
            Level = level;
            // Stats at lvl 1
            Stat = new Stat
            {
                Health = 150,
                Strength = 10,
                Dexterity = 3,
                Intelligence = 1
            };

            // Base stats template to get original value, used when lvl up
            BaseStat = new Stat
            {
                Health = 150,
                Strength = 10,
                Dexterity = 3,
                Intelligence = 1
            };

            // Increased stats on level up (constant)
            LevelUp = new Stat
            {
                Health = 30,
                Strength = 5,
                Dexterity = 2,
                Intelligence = 1
            };
        }

    }
}
