﻿using assignment_2_rpg_characters.Models.Stats;

namespace assignment_2_rpg_characters.Models.Heroes
{
    public class Ranger : Hero
    {
        //level can be sent as a value for easier debugging and testing
        public Ranger(string name, int level = 1)
        {
            Name = name;
            Level = level;

            //Base stats lvl 1 on creation
            Stat = new Stat
            {
                Health = 120,
                Strength = 5,
                Dexterity = 10,
                Intelligence = 2,
            };

            BaseStat = new Stat
            {
                Health = 120,
                Strength = 5,
                Dexterity = 10,
                Intelligence = 2,
            };

            // Increased stats on level up
            LevelUp = new Stat
            {
                Health = 20,
                Strength = 2,
                Dexterity = 5,
                Intelligence = 1
            };
        }
    }
}
