﻿using assignment_2_rpg_characters.Interfaces;
using System;
using System.Collections.Generic;

namespace assignment_2_rpg_characters.Models.Heroes
{
    public class Hero : IHero
    {
        public const int MAXLEVEL = 100;
        public string Name { get; set; }
        public List<ISlot> Slots { get; set; } = new List<ISlot>();
        public IStat Stat { get; set; }
        public IStat BaseStat { get; set; }
        public IStat LevelUp { get; set; }
        public int Level { get; set; }
        public int Experience { get; set; }
        public int Damage { get; set; }
        public int ExperienceToNextLevel { get; set; }

        public Hero()
        {
            ExperienceToNextLevel = 100;
        }
        
        public void ResetStats()
        {
            Stat.Health -= BaseStat.Health;
            Stat.Dexterity -= BaseStat.Dexterity;
            Stat.Strength -= BaseStat.Strength ;
            Stat.Intelligence -= BaseStat.Intelligence;
        }

        public void UpdateStats()
        {
            ResetStats();

            Stat.Health += LevelUp.Health * (Level - 1);
            Stat.Dexterity += LevelUp.Dexterity * (Level - 1);
            Stat.Strength += LevelUp.Strength * (Level - 1);
            Stat.Intelligence += LevelUp.Intelligence * (Level - 1);
        }
        public int GetLevelCap(int level)
        {
           // I really dont like this way of leveling up as described in the assignment
           // The math should be something like this, Math.Floor(100 * 1.1, level) and iterate trought it, 
           // then i could just couldve had this function inside AddExp without having to save the the previous rounded down value
            var levelCap = (int)Math.Floor(100 * Math.Pow(1.1, 0));
            for (int i = 0; i < level; i++)
            {
                levelCap = (int)Math.Floor(1.1 * levelCap);
            }
            return levelCap;
        }
        public void AddExp(int exp)
        {
            if (exp <= 0) return;
            var newExp = exp;
            var newLevel = Level;
            for (int i = Level-1; i < MAXLEVEL; i++)
            {
                var levelCap = GetLevelCap(i);
                if (Experience + newExp >= levelCap)
                {
                    newLevel++;
                    newExp -= levelCap;
                }
                else
                {   

                    //Update Hero stats to new values
                    Level = newLevel;
                    Experience += newExp;
                    ExperienceToNextLevel = levelCap - Experience;
                    UpdateStats();
                    return;
                }
            }
        }

        public override string ToString()
        {
            return $"Hero details:\n" +
                    $"Name: {Name}\n" +
                    $"HP: {Stat.Health}\n" +
                    $"Str: {Stat.Strength}\n" +
                    $"Dex: {Stat.Dexterity}\n" +
                    $"Int: {Stat.Intelligence}\n" +
                    $"Lvl: {Level}\n" +
                    $"XP to next: {ExperienceToNextLevel}";
        }
    }
}
