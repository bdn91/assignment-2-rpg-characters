﻿using assignment_2_rpg_characters.Enums;
using assignment_2_rpg_characters.Models.Stats;
using System;

namespace assignment_2_rpg_characters.Models.Armors
{
    public class Leather : Armor
    {
        public Leather(string name, SlotType slotType, int level = 1)
        {
            Name = name;
            ArmorType = ArmorType.Leather;
            RequiredLevel = level;
            SlotType = slotType;
            Scale = GetScaling();

            // Bonus stats based on cloth buff and scaling
            Bonus = new Stat
            {
                Health = (int)Math.Floor( (20 + (8 * level) ) * Scale),
                Dexterity = (int)Math.Floor( (3 + (2 * level) ) * Scale),
                Strength = (int)Math.Floor( (1 + (1 * level) ) * Scale)
            };
        }
    }
}
