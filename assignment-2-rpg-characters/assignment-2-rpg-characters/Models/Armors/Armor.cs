﻿using assignment_2_rpg_characters.Interfaces;
using assignment_2_rpg_characters.Enums;
using System.Collections.Generic;

namespace assignment_2_rpg_characters.Models.Armors
{
    public class Armor : Slot, IArmor
    {
        public string Name { get; set; }
        public ArmorType ArmorType { get; set; }
        public double Scale { get; set; }

        public Armor()
        {
   
        }

        public double GetScaling() 
        {
            var scaleRules = new Dictionary<SlotType, double> {
                {SlotType.Head,  0.8},
                {SlotType.Body,  1},
                {SlotType.Legs,  0.6}
            };
            return scaleRules.ContainsKey(SlotType) ? scaleRules[SlotType] : 1;
        }
        

        public override string ToString()
        {
            return $"Item stat for: {Name}\n" +
                    $"Armor Type: {ArmorType}\n" +
                    $"Slot: {SlotType}\n" +
                    $"Armor Level: {RequiredLevel}\n" +
                    $"Bonus HP: {Bonus.Health}\n" +
                    $"Bonus STR: {Bonus.Strength}\n" +
                    $"Bonus INT for: {Bonus.Intelligence}\n" +
                    $"Bonus DEX: {Bonus.Dexterity}\n";

        }
    }
}
