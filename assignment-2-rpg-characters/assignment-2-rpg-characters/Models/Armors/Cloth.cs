﻿using assignment_2_rpg_characters.Enums;
using assignment_2_rpg_characters.Models.Stats;
using System;

namespace assignment_2_rpg_characters.Models.Armors
{
    public class Cloth : Armor
    {
        public Cloth(string name, SlotType slotType, int level = 1)
        {
            Name = name;
            ArmorType = ArmorType.Cloth;
            SlotType = slotType;
            RequiredLevel = level;
            Scale = GetScaling();
            

            // Bonus stats based on cloth buff and scaling
            Bonus = new Stat
            {
                
                Health = (int)Math.Floor((10 + (5 * level)) * Scale),
                Dexterity = (int)Math.Floor((1 + (1 * level)) * Scale),
                Intelligence = (int)Math.Floor((3 + (2 * level)) * Scale)
            };
        }

    }
}
