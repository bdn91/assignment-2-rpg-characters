﻿namespace assignment_2_rpg_characters.Enums
{
    public enum WeaponType
    {
        Ranged,
        Melee,
        Magic
    }
}
