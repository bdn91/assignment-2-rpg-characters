﻿namespace assignment_2_rpg_characters.Enums
{
    public enum ArmorType
    {
        Cloth,
        Leather,
        Plate
    }
}
