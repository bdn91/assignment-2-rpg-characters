﻿namespace assignment_2_rpg_characters.Enums
{
    public enum SlotType
    {
        Weapon,
        Body,
        Head,
        Legs
    }
}
