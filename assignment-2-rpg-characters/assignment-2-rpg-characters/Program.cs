﻿using assignment_2_rpg_characters.Builder;
using assignment_2_rpg_characters.Enums;
using assignment_2_rpg_characters.Models.Armors;
using assignment_2_rpg_characters.Models.Heroes;
using assignment_2_rpg_characters.Models.Weapons;
using System;

namespace assignment_2_rpg_characters
{
    class Program
    {
        static void Main(string[] args)
        {
            //creating hero
            var hero = new Warrior("Myamoto Musashi");
            Console.WriteLine(hero);

            //creating weapons
            var sword = new MeleeWeapon("Sword", 1);
            var bow = new RangedWeapon("Bow", 1);
            var staff = new MagicWeapon("Staff", 1);

            //showing weapons
            Console.WriteLine(sword);
            Console.WriteLine();
            Console.WriteLine(bow);
            Console.WriteLine();
            Console.WriteLine(staff);

            //creating armors
            var headCloth = new Cloth("Head cloth", SlotType.Head, 1);
            var bodyPlate = new Plate("Body plate", SlotType.Body, 1);
            var legsLeather = new Leather("Legs leather", SlotType.Legs, 1);

            //showing armors
            Console.WriteLine(headCloth);
            Console.WriteLine();
            Console.WriteLine(bodyPlate);
            Console.WriteLine();
            Console.WriteLine(legsLeather);

            var heroBuilder = new HeroBuilder();
            heroBuilder.AddSlot(hero, headCloth);
            
            Console.WriteLine(hero.Damage);

            heroBuilder.AddSlot(hero, bodyPlate);
            heroBuilder.AddSlot(hero, sword);
            Console.WriteLine(hero.Damage);


            Console.WriteLine(hero);





            // Hero should be lvl 9
            hero.AddExp(1139);
            // Hero should be lvl 10
            hero.AddExp(230);
            // Hero should be at level 5 with 0xp
            Console.WriteLine(hero.Damage);

            Console.WriteLine(hero);
        }
    }
}
