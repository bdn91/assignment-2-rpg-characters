﻿using assignment_2_rpg_characters.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using assignment_2_rpg_characters.Enums;

namespace assignment_2_rpg_characters.Builder
{
    /// <summary>
    /// Class for adding and removing armor and weapons
    /// Adding and removing bonus hp/damage and xp
    /// </summary>
    public class HeroBuilder
    {
        public void AddSlots(IHero hero, IEnumerable<ISlot> slots)
        {
            foreach (var slot in slots)
            {
                AddSlot(hero, slot);
            }
        }

        public void AddSlot(IHero hero, ISlot slot)
        {
            
            if (hero.Level < slot.RequiredLevel)
            {
                Console.WriteLine("Your level is to low to equip this armor");
                return;
            }
            // If armor on the slot exists, it will be replaced
            var slotToReplace = hero.Slots.FirstOrDefault(x => x.SlotType == slot.SlotType);
            // if null nothing to remove
            RemoveSlot(hero, slotToReplace);
            hero.Slots.Add(slot);
            AddBonusStats(hero, slot, 1);            
            AddDamange(hero, slot as IWeapon);
        }
        public void RemoveSlot(IHero hero, ISlot slot)
        {
            // if null nothing to remove
            if (slot == null) return;
            // Returns items removed in list
            var removed = hero.Slots.RemoveAll(x => x.SlotType == slot.SlotType);
            AddBonusStats(hero, slot, -1 * removed);
        }

        public void ClearSlots(IHero hero)
        {
            foreach (var slot in hero.Slots.ToList())
            {
                RemoveSlot(hero, slot);
            }
        }

        // Adds armor from hero. Bonus increase is constant times multiplier(armors added)
        public void AddBonusStats(IHero hero, ISlot slot, int multiplier)
        {
            hero.Stat.Health += slot.Bonus.Health * multiplier;
            hero.Stat.Intelligence += slot.Bonus.Intelligence * multiplier;
            hero.Stat.Dexterity += slot.Bonus.Dexterity * multiplier;
            hero.Stat.Strength += slot.Bonus.Strength * multiplier;
        }


        public void AddDamange(IHero hero, IWeapon weapon)
        {
            if (weapon == null) return;
            var damageRules = new Dictionary<WeaponType, int>
            {
                {WeaponType.Melee,  hero.Stat.Strength},
                {WeaponType.Magic,  hero.Stat.Intelligence},
                {WeaponType.Ranged,  hero.Stat.Dexterity}
            };

            var additionalDamage = damageRules.ContainsKey(weapon.WeaponType) ? damageRules[weapon.WeaponType] : 1;
            hero.Damage += weapon.Damage + (int)Math.Floor(weapon.Scale * additionalDamage);
        }
    }
}
