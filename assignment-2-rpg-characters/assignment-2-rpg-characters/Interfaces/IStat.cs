﻿namespace assignment_2_rpg_characters.Interfaces
{
    public interface IStat
    {
        int Health { get; set; }
        int Strength { get; set; }
        int Dexterity { get; set; }
        int Intelligence { get; set; }
    }
}
