﻿using assignment_2_rpg_characters.Enums;

namespace assignment_2_rpg_characters.Interfaces
{
    public interface ISlot
    {
        SlotType SlotType { get; set; }
        int RequiredLevel { get; set; }
        IStat Bonus { get; set; }
    }
}