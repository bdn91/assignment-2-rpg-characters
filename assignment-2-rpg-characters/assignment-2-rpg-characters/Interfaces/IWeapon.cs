﻿using assignment_2_rpg_characters.Enums;

namespace assignment_2_rpg_characters.Interfaces
{
    public interface IWeapon
    {
        public WeaponType WeaponType { get; set; }
        public int Damage { get; set; }
        double Scale { get; set; }
    }
}
