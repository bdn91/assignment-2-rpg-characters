﻿using assignment_2_rpg_characters.Enums;

namespace assignment_2_rpg_characters.Interfaces
{
    interface IArmor
    {
        string Name { get; set; }
        ArmorType ArmorType { get; set; }
    }
}
