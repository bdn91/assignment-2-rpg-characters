﻿using System.Collections.Generic;

namespace assignment_2_rpg_characters.Interfaces
{
    public interface IHero
    {
        string Name { get; set; }
        public List<ISlot> Slots { get; set; } 
        public IStat Stat { get; set; }
        public IStat BaseStat { get; set; }
        public IStat LevelUp { get; set; }
        public int Level { get; set; }
        public int Experience { get; set; }
        public int Damage { get; set; }
    }
}
