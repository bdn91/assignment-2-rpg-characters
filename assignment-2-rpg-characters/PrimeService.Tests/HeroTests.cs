using assignment_2_rpg_characters.Models.Heroes;
using System;
using Xunit;

namespace RPG.Tests
{
    public class HeroTests
    {
        [Fact]
        public void Level1Test()
        {
            var hero = new Warrior("Myamoto Musashi");
            var remainingExperiencePoints = hero.ExperienceToNextLevel;
            Assert.True(remainingExperiencePoints == 100, $"{remainingExperiencePoints}");
        }

        [Fact]
        public void Level2Test()
        {
            var hero = new Warrior("Myamoto Musashi");
            hero.AddExp(100);
            var remainingExperiencePoints = hero.ExperienceToNextLevel;
            Assert.True(remainingExperiencePoints == 110, $"{remainingExperiencePoints}");
        }

        [Fact]
        public void Level3Test()
        {
            var hero = new Warrior("Myamoto Musashi");
            hero.AddExp(210);
            var remainingExperiencePoints = hero.ExperienceToNextLevel;

            Assert.True(remainingExperiencePoints == 121, $"{remainingExperiencePoints}");
        }


        [Fact]
        public void CheckRemainingExperincePoints()
        {
            var hero = new Warrior("Myamoto Musashi");
            hero.AddExp(90);
            var remainingExperiencePoints = hero.ExperienceToNextLevel;
            Assert.True(remainingExperiencePoints == 10, $"{remainingExperiencePoints}");
        }
        [Fact]
        public void ChecekRemainingExperincePoints()
        {
            // Arrange
            // Hero is level 1
            var hero = new Warrior("Myamoto Musashi");
            
            //Act
            // 100 to level 2
            // 110 to level 3
            // 121 to level 4
            hero.AddExp(210);
            // Hero should be at level 3 with 0 xp and 121xp left to level up
            var remainingExperiencePoints = hero.ExperienceToNextLevel;
            Assert.True(remainingExperiencePoints == 121, $"{remainingExperiencePoints}");
            Assert.True(hero.Experience == 0);
        }

        [Fact]
        public void WarriorCheckStatsAtLevel9()
        {
            // Arrange
            // Hero is level 1
            var hero = new Warrior("Myamoto Musashi");

            //Act
            hero.AddExp(465);
            // Hero should be at level 5 with 464xp
            // Hero should have 1 xp transferred from previous level

            var level = hero.Level;

            Assert.True(hero.Experience == 1, $"{hero.Experience}");
            Assert.True(hero.Level == 5, $"{hero.Level}");

        }
    }
}
