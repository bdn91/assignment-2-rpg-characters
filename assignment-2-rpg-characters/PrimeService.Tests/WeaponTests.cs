using System;
using assignment_2_rpg_characters.Builder;
using assignment_2_rpg_characters.Enums;
using assignment_2_rpg_characters.Models.Armors;
using assignment_2_rpg_characters.Models.Heroes;
using assignment_2_rpg_characters.Models.Weapons;
using Xunit;

namespace RPG.Tests
{
    public class WeaponTests
    {
        [Fact]
        public void AddMeleeWithoutArmor()
        {
            var heroBuilder = new HeroBuilder();
            var hero = new Warrior("Myamoto Musashi", 15);
            var weaponLevel = 10;
            var weapon = new MeleeWeapon("Sword", weaponLevel);
            heroBuilder.AddSlot(hero, weapon);
            var weaponDamage = 15 + 2 * weaponLevel + 1.5 * hero.Stat.Strength;
            Assert.True(hero.Damage == 50, $"Weapon damage should be {hero.Damage} but is {weaponDamage}");
        }
    }
}
