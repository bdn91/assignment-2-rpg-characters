﻿
using System;
using assignment_2_rpg_characters.Builder;
using assignment_2_rpg_characters.Enums;
using assignment_2_rpg_characters.Models.Armors;
using assignment_2_rpg_characters.Models.Heroes;
using assignment_2_rpg_characters.Models.Weapons;
using Xunit;

namespace RPG.Tests
{
    public class ArmorTests
    {
        [Fact]
        public void CreateClothHeadLvl1()
        {
            //Arrange
            //Base bonus
            var level = 1;
            var baseBonusHP = 10;
            var baseBonusInt = 3;
            var baseBonusDex = 1;
            var baseBonusStr = 0;
            // Scaling by armor level on cloth
            var hpScaling = 5;
            var intScaling = 2;
            var dexScaling = 1;
            var strScaling = 0;
            // Scaling by slot type (body part)
            var slotScaling = 0.8;
            // Formula for bonus stats on armor
            var bonusHP = (int)Math.Floor((baseBonusHP + hpScaling * level) * slotScaling);
            var bonusInt = (int)Math.Floor((baseBonusInt + intScaling * level) * slotScaling);
            var bonusDex = (int)Math.Floor((baseBonusDex + dexScaling * level) * slotScaling);
            var bonusStr = (int)Math.Floor((baseBonusStr + strScaling * level) * slotScaling);

            //Act
            var armor = new Cloth("Head cloth", SlotType.Head);
            
            //Assert
            Assert.Equal(bonusHP, armor.Bonus.Health);
            Assert.Equal(bonusInt, armor.Bonus.Intelligence);
            Assert.Equal(bonusDex, armor.Bonus.Dexterity);
            Assert.Equal(bonusStr, armor.Bonus.Strength);
        }

        [Fact]
        public void CreateClothHeadLvl2()
        {
            //Arrange
            //Base bonus
            var level = 2;
            var baseBonusHP = 10;
            var baseBonusInt = 3;
            var baseBonusDex = 1;
            var baseBonusStr = 0;
            // Scaling by armor level on cloth
            var hpScaling = 5;
            var intScaling = 2;
            var dexScaling = 1;
            var strScaling = 0;
            // Scaling by slot type (body part)
            var slotScaling = 0.8;
            // Formula for bonus stats on armor
            var bonusHP = (int)Math.Floor((baseBonusHP + hpScaling * level) * slotScaling);
            var bonusInt = (int)Math.Floor((baseBonusInt + intScaling * level) * slotScaling);
            var bonusDex = (int)Math.Floor((baseBonusDex + dexScaling * level) * slotScaling);
            var bonusStr = (int)Math.Floor((baseBonusStr + strScaling * level) * slotScaling);

            //Act
            // Setting optional parameter, level 2
            var armor = new Cloth("Head cloth", SlotType.Head, 2);
            //Assert
            Assert.Equal(bonusHP, armor.Bonus.Health);
            Assert.Equal(bonusInt, armor.Bonus.Intelligence);
            Assert.Equal(bonusDex, armor.Bonus.Dexterity);
            Assert.Equal(bonusStr, armor.Bonus.Strength);
        }

        [Fact]
        public void CreateClothBodyLvl1()
        {
            //Arrange
            //Base bonus
            var level = 1;
            var baseBonusHP = 10;
            var baseBonusInt = 3;
            var baseBonusDex = 1;
            var baseBonusStr = 0;
            // Scaling by armor level on cloth
            var hpScaling = 5;
            var intScaling = 2;
            var dexScaling = 1;
            var strScaling = 0;
            // Scaling by slot type (body part)
            var slotScaling = 1.0;
            // Formula for bonus stats on armor
            var bonusHP = (int)Math.Floor((baseBonusHP + hpScaling * level) * slotScaling);
            var bonusInt = (int)Math.Floor((baseBonusInt + intScaling * level) * slotScaling);
            var bonusDex = (int)Math.Floor((baseBonusDex + dexScaling * level) * slotScaling);
            var bonusStr = (int)Math.Floor((baseBonusStr + strScaling * level) * slotScaling);

            //Act
            // Setting optional parameter, level 2
            var armor = new Cloth("Body cloth", SlotType.Body);

            //Assert
            Assert.Equal(bonusHP, armor.Bonus.Health);
            Assert.Equal(bonusInt, armor.Bonus.Intelligence);
            Assert.Equal(bonusDex, armor.Bonus.Dexterity);
            Assert.Equal(bonusStr, armor.Bonus.Strength);
        }

        [Fact]
        public void CreateClothBodyLvl2()
        {
            //Arrange
            //Base bonus
            var level = 2;
            var baseBonusHP = 10;
            var baseBonusInt = 3;
            var baseBonusDex = 1;
            var baseBonusStr = 0;
            // Scaling by armor level on cloth
            var hpScaling = 5;
            var intScaling = 2;
            var dexScaling = 1;
            var strScaling = 0;
            // Scaling by slot type (body part)
            var slotScaling = 1.0;
            // Formula for bonus stats on armor
            var bonusHP = (int)Math.Floor((baseBonusHP + hpScaling * level) * slotScaling);
            var bonusInt = (int)Math.Floor((baseBonusInt + intScaling * level) * slotScaling);
            var bonusDex = (int)Math.Floor((baseBonusDex + dexScaling * level) * slotScaling);
            var bonusStr = (int)Math.Floor((baseBonusStr + strScaling * level) * slotScaling);
            //Act
            // Setting optional parameter, level 2
            var armor = new Cloth("Body cloth", SlotType.Body, 2);

            //Assert
            Assert.Equal(bonusHP, armor.Bonus.Health);
            Assert.Equal(bonusInt, armor.Bonus.Intelligence);
            Assert.Equal(bonusDex, armor.Bonus.Dexterity);
            Assert.Equal(bonusStr, armor.Bonus.Strength);
        }

        [Fact]
        public void CreateClothLegsLvl1()
        {
            //Arrange
            //Base bonus
            var level = 1;
            var baseBonusHP = 10;
            var baseBonusInt = 3;
            var baseBonusDex = 1;
            var baseBonusStr = 0;
            // Scaling by armor level on cloth
            var hpScaling = 5;
            var intScaling = 2;
            var dexScaling = 1;
            var strScaling = 0;
            // Scaling by slot type (body part)
            var scaling = 0.6;
            // Formula for bonus stats on armor
            var bonusHP = (int)Math.Floor((baseBonusHP + hpScaling * level) * scaling);
            var bonusInt = (int)Math.Floor((baseBonusInt + intScaling * level) * scaling);
            var bonusDex = (int)Math.Floor((baseBonusDex + dexScaling * level) * scaling);
            var bonusStr = (int)Math.Floor((baseBonusStr + strScaling * level) * scaling);
            //Act
            var armor = new Cloth("Head cloth", SlotType.Legs);

            //Assert
            Assert.Equal(bonusHP, armor.Bonus.Health);
            Assert.Equal(bonusInt, armor.Bonus.Intelligence);
            Assert.Equal(bonusDex, armor.Bonus.Dexterity);
            Assert.Equal(bonusStr, armor.Bonus.Strength);
        }

        [Fact]
        public void CreateClothLegsLvl2()
        {
            //Arrange
            //Base bonus
            var level = 2;
            var baseBonusHP = 10;
            var baseBonusInt = 3;
            var baseBonusDex = 1;
            var baseBonusStr = 0;
            // Scaling by armor level on cloth
            var hpScaling = 5;
            var intScaling = 2;
            var dexScaling = 1;
            var strScaling = 0;
            // Scaling by slot type (body part)
            var scaling = 0.6;
            // Formula for bonus stats on armor
            var bonusHP = (int)Math.Floor((baseBonusHP + hpScaling * level) * scaling);
            var bonusInt = (int)Math.Floor((baseBonusInt + intScaling * level) * scaling);
            var bonusDex = (int)Math.Floor((baseBonusDex + dexScaling * level) * scaling);
            var bonusStr = (int)Math.Floor((baseBonusStr + strScaling * level) * scaling);
            //Act
            // Setting optional parameter, level 2
            var armor = new Cloth("Leg cloth", SlotType.Legs, 2);

            //Assert
            Assert.Equal(bonusHP, armor.Bonus.Health);
            Assert.Equal(bonusInt, armor.Bonus.Intelligence);
            Assert.Equal(bonusDex, armor.Bonus.Dexterity);
            Assert.Equal(bonusStr, armor.Bonus.Strength);
        }

        [Fact]
        public void CreatePlateHeadLvl1()
        {
            //Arrange
            //Base bonus
            var level = 1;
            var baseBonusHP = 30;
            var baseBonusInt = 0;
            var baseBonusDex = 1;
            var baseBonusStr = 3;
            // Scaling by armor level on cloth
            var hpScaling = 12;
            var intScaling = 0;
            var dexScaling = 1;
            var strScaling = 2;
            // Scaling by slot type (body part)
            var slotScaling = 0.8;
            // Formula for bonus stats on armor
            var bonusHP = (int)Math.Floor((baseBonusHP + hpScaling * level) * slotScaling);
            var bonusInt = (int)Math.Floor((baseBonusInt + intScaling * level) * slotScaling);
            var bonusDex = (int)Math.Floor((baseBonusDex + dexScaling * level) * slotScaling);
            var bonusStr = (int)Math.Floor((baseBonusStr + strScaling * level) * slotScaling);

            //Act
            var armor = new Plate("Head Plate", SlotType.Head);
            
            //Assert
            Assert.Equal(bonusHP, armor.Bonus.Health);
            Assert.Equal(bonusInt, armor.Bonus.Intelligence);
            Assert.Equal(bonusDex, armor.Bonus.Dexterity);
            Assert.Equal(bonusStr, armor.Bonus.Strength);
        }

        [Fact]
        public void CreatePlateHeadLvl2()
        {
            //Arrange
            //Base bonus
            var level = 2;
            var baseBonusHP = 30;
            var baseBonusInt = 0;
            var baseBonusDex = 1;
            var baseBonusStr = 3;
            // Scaling by armor level on cloth
            var hpScaling = 12;
            var intScaling = 0;
            var dexScaling = 1;
            var strScaling = 2;
            // Scaling by slot type (body part)
            var slotScaling = 0.8;
            // Formula for bonus stats on armor
            var bonusHP = (int)Math.Floor((baseBonusHP + hpScaling * level) * slotScaling);
            var bonusInt = (int)Math.Floor((baseBonusInt + intScaling * level) * slotScaling);
            var bonusDex = (int)Math.Floor((baseBonusDex + dexScaling * level) * slotScaling);
            var bonusStr = (int)Math.Floor((baseBonusStr + strScaling * level) * slotScaling);

            //Act
            // Setting optional parameter, level 2
            var armor = new Plate("Head Plate", SlotType.Head, 2);

            //Assert
            Assert.Equal(bonusHP, armor.Bonus.Health);
            Assert.Equal(bonusInt, armor.Bonus.Intelligence);
            Assert.Equal(bonusDex, armor.Bonus.Dexterity);
            Assert.Equal(bonusStr, armor.Bonus.Strength);
        }

        [Fact]
        public void CreatePlateBodyLvl1()
        {
            //Arrange
            //Base bonus
            var level = 1;
            var baseBonusHP = 30;
            var baseBonusInt = 0;
            var baseBonusDex = 1;
            var baseBonusStr = 3;
            // Scaling by armor level on cloth
            var hpScaling = 12;
            var intScaling = 0;
            var dexScaling = 1;
            var strScaling = 2;
            // Scaling by slot type (body part)
            var slotScaling = 1.0;
            // Formula for bonus stats on armor
            var bonusHP = (int)Math.Floor((baseBonusHP + hpScaling * level) * slotScaling);
            var bonusInt = (int)Math.Floor((baseBonusInt + intScaling * level) * slotScaling);
            var bonusDex = (int)Math.Floor((baseBonusDex + dexScaling * level) * slotScaling);
            var bonusStr = (int)Math.Floor((baseBonusStr + strScaling * level) * slotScaling);

            //Act
            var armor = new Plate("Body Plate", SlotType.Body);

            //Assert
            Assert.Equal(bonusHP, armor.Bonus.Health);
            Assert.Equal(bonusInt, armor.Bonus.Intelligence);
            Assert.Equal(bonusDex, armor.Bonus.Dexterity);
            Assert.Equal(bonusStr, armor.Bonus.Strength);
        }

        [Fact]
        public void CreatePlateBodyLvl2()
        {
            //Arrange
            //Base bonus
            var level = 2;
            var baseBonusHP = 30;
            var baseBonusInt = 0;
            var baseBonusDex = 1;
            var baseBonusStr = 3;
            // Scaling by armor level on cloth
            var hpScaling = 12;
            var intScaling = 0;
            var dexScaling = 1;
            var strScaling = 2;
            // Scaling by slot type (body part)
            var slotScaling = 1.0;
            // Formula for bonus stats on armor
            var bonusHP = (int)Math.Floor((baseBonusHP + hpScaling * level) * slotScaling);
            var bonusInt = (int)Math.Floor((baseBonusInt + intScaling * level) * slotScaling);
            var bonusDex = (int)Math.Floor((baseBonusDex + dexScaling * level) * slotScaling);
            var bonusStr = (int)Math.Floor((baseBonusStr + strScaling * level) * slotScaling);

            //Act
            var armor = new Plate("Body Plate", SlotType.Body, 2);

            //Assert
            Assert.Equal(bonusHP, armor.Bonus.Health);
            Assert.Equal(bonusInt, armor.Bonus.Intelligence);
            Assert.Equal(bonusDex, armor.Bonus.Dexterity);
            Assert.Equal(bonusStr, armor.Bonus.Strength);
        }

        [Fact]
        public void CreatePlateLegsLvl1()
        {
            //Arrange
            //Base bonus
            var level = 1;
            var baseBonusHP = 30;
            var baseBonusInt = 0;
            var baseBonusDex = 1;
            var baseBonusStr = 3;
            // Scaling by armor level on cloth
            var hpScaling = 12;
            var intScaling = 0;
            var dexScaling = 1;
            var strScaling = 2;
            // Scaling by slot type (body part)
            var slotScaling = 0.6;
            // Formula for bonus stats on armor
            var bonusHP = (int)Math.Floor((baseBonusHP + hpScaling * level) * slotScaling);
            var bonusInt = (int)Math.Floor((baseBonusInt + intScaling * level) * slotScaling);
            var bonusDex = (int)Math.Floor((baseBonusDex + dexScaling * level) * slotScaling);
            var bonusStr = (int)Math.Floor((baseBonusStr + strScaling * level) * slotScaling);

            //Act
            var armor = new Plate("Legs Plate", SlotType.Legs);

            //Assert
            Assert.Equal(bonusHP, armor.Bonus.Health);
            Assert.Equal(bonusInt, armor.Bonus.Intelligence);
            Assert.Equal(bonusDex, armor.Bonus.Dexterity);
            Assert.Equal(bonusStr, armor.Bonus.Strength);
        }

        [Fact]
        public void CreatePlateLegsLvl2()
        {
            //Arrange
            //Base bonus
            var level = 2;
            var baseBonusHP = 30;
            var baseBonusInt = 0;
            var baseBonusDex = 1;
            var baseBonusStr = 3;
            // Scaling by armor level on cloth
            var hpScaling = 12;
            var intScaling = 0;
            var dexScaling = 1;
            var strScaling = 2;
            // Scaling by slot type (body part)
            var slotScaling = 0.6;
            // Formula for bonus stats on armor
            var bonusHP = (int)Math.Floor((baseBonusHP + hpScaling * level) * slotScaling);
            var bonusInt = (int)Math.Floor((baseBonusInt + intScaling * level) * slotScaling);
            var bonusDex = (int)Math.Floor((baseBonusDex + dexScaling * level) * slotScaling);
            var bonusStr = (int)Math.Floor((baseBonusStr + strScaling * level) * slotScaling);

            //Act
            var armor = new Plate("Legs Plate", SlotType.Legs, 2);

            //Assert
            Assert.Equal(bonusHP, armor.Bonus.Health);
            Assert.Equal(bonusInt, armor.Bonus.Intelligence);
            Assert.Equal(bonusDex, armor.Bonus.Dexterity);
            Assert.Equal(bonusStr, armor.Bonus.Strength);
        }

        [Fact]
        public void CreateLeatherHeadLvl1()
        {
            //Arrange
            //Base bonus
            var level = 1;
            var baseBonusHP = 20;
            var baseBonusInt = 0;
            var baseBonusDex = 3;
            var baseBonusStr = 1;
            // Scaling by armor level on cloth
            var hpScaling = 8;
            var intScaling = 0;
            var dexScaling = 2;
            var strScaling = 1;
            // Scaling by slot type (body part)
            var slotScaling = 0.8;
            // Formula for bonus stats on armor
            var bonusHP = (int)Math.Floor((baseBonusHP + hpScaling * level) * slotScaling);
            var bonusInt = (int)Math.Floor((baseBonusInt + intScaling * level) * slotScaling);
            var bonusDex = (int)Math.Floor((baseBonusDex + dexScaling * level) * slotScaling);
            var bonusStr = (int)Math.Floor((baseBonusStr + strScaling * level) * slotScaling);
           
            //Act
            var armor = new Leather("Head Leather", SlotType.Head);

            //Assert
            Assert.Equal(bonusHP, armor.Bonus.Health);
            Assert.Equal(bonusInt, armor.Bonus.Intelligence);
            Assert.Equal(bonusDex, armor.Bonus.Dexterity);
            Assert.Equal(bonusStr, armor.Bonus.Strength);
        }

        [Fact]
        public void CreateLeatherHeadLvl2()
        {
            //Arrange
            //Base bonus
            var level = 2;
            var baseBonusHP = 20;
            var baseBonusInt = 0;
            var baseBonusDex = 3;
            var baseBonusStr = 1;
            // Scaling by armor level on cloth
            var hpScaling = 8;
            var intScaling = 0;
            var dexScaling = 2;
            var strScaling = 1;
            // Scaling by slot type (body part)
            var slotScaling = 0.8;
            // Formula for bonus stats on armor
            var bonusHP = (int)Math.Floor((baseBonusHP + hpScaling * level) * slotScaling);
            var bonusInt = (int)Math.Floor((baseBonusInt + intScaling * level) * slotScaling);
            var bonusDex = (int)Math.Floor((baseBonusDex + dexScaling * level) * slotScaling);
            var bonusStr = (int)Math.Floor((baseBonusStr + strScaling * level) * slotScaling);

            //Act
            // Setting optional parameter, level 2
            var armor = new Leather("Head Leather", SlotType.Head, 2);

            //Assert
            Assert.Equal(bonusHP, armor.Bonus.Health);
            Assert.Equal(bonusInt, armor.Bonus.Intelligence);
            Assert.Equal(bonusDex, armor.Bonus.Dexterity);
            Assert.Equal(bonusStr, armor.Bonus.Strength);
        }

        [Fact]
        public void CreateLeatherBodyLvl1()
        {
            //Arrnage
            //Base bonus
            var level = 1;
            var baseBonusHP = 20;
            var baseBonusInt = 0;
            var baseBonusDex = 3;
            var baseBonusStr = 1;
            // Scaling by armor level on cloth
            var hpScaling = 8;
            var intScaling = 0;
            var dexScaling = 2;
            var strScaling = 1;
            // Scaling by slot type (body part)
            var slotScaling = 1.0;
            // Formula for bonus stats on armor
            var bonusHP = (int)Math.Floor((baseBonusHP + hpScaling * level) * slotScaling);
            var bonusInt = (int)Math.Floor((baseBonusInt + intScaling * level) * slotScaling);
            var bonusDex = (int)Math.Floor((baseBonusDex + dexScaling * level) * slotScaling);
            var bonusStr = (int)Math.Floor((baseBonusStr + strScaling * level) * slotScaling);

            //Act
            var armor = new Leather("Body Leather", SlotType.Body);

            //Assert
            Assert.Equal(bonusHP, armor.Bonus.Health);
            Assert.Equal(bonusInt, armor.Bonus.Intelligence);
            Assert.Equal(bonusDex, armor.Bonus.Dexterity);
            Assert.Equal(bonusStr, armor.Bonus.Strength);
        }
        [Fact]
        public void CreateLeatherBodyLvl2()
        {
            //Arrange
            //Base bonus
            var level = 2;
            var baseBonusHP = 20;
            var baseBonusInt = 0;
            var baseBonusDex = 3;
            var baseBonusStr = 1;
            // Scaling by armor level on cloth
            var hpScaling = 8;
            var intScaling = 0;
            var dexScaling = 2;
            var strScaling = 1;
            // Scaling by slot type (body part)
            var slotScaling = 1.0;
            // Formula for bonus stats on armor
            var bonusHP = (int)Math.Floor((baseBonusHP + hpScaling * level) * slotScaling);
            var bonusInt = (int)Math.Floor((baseBonusInt + intScaling * level) * slotScaling);
            var bonusDex = (int)Math.Floor((baseBonusDex + dexScaling * level) * slotScaling);
            var bonusStr = (int)Math.Floor((baseBonusStr + strScaling * level) * slotScaling);

            //Act
            // Setting optional parameter, level 2
            var armor = new Leather("Body Leather", SlotType.Body, 2);

            //Assert
            Assert.Equal(bonusHP, armor.Bonus.Health);
            Assert.Equal(bonusInt, armor.Bonus.Intelligence);
            Assert.Equal(bonusDex, armor.Bonus.Dexterity);
            Assert.Equal(bonusStr, armor.Bonus.Strength);
        }

        [Fact]
        public void CreateLeatherLegsLvl1()
        {
            //Arrange
            //Base bonus
            var level = 1;
            var baseBonusHP = 20;
            var baseBonusInt = 0;
            var baseBonusDex = 3;
            var baseBonusStr = 1;
            // Scaling by armor level on cloth
            var hpScaling = 8;
            var intScaling = 0;
            var dexScaling = 2;
            var strScaling = 1;
            // Scaling by slot type (body part)
            var slotScaling = 0.6;
            // Formula for bonus stats on armor
            var bonusHP = (int)Math.Floor((baseBonusHP + hpScaling * level) * slotScaling);
            var bonusInt = (int)Math.Floor((baseBonusInt + intScaling * level) * slotScaling);
            var bonusDex = (int)Math.Floor((baseBonusDex + dexScaling * level) * slotScaling);
            var bonusStr = (int)Math.Floor((baseBonusStr + strScaling * level) * slotScaling);

            //Act
            var armor = new Leather("Legs Leather", SlotType.Legs);

            //Assert
            Assert.Equal(bonusHP, armor.Bonus.Health);
            Assert.Equal(bonusInt, armor.Bonus.Intelligence);
            Assert.Equal(bonusDex, armor.Bonus.Dexterity);
            Assert.Equal(bonusStr, armor.Bonus.Strength);
        }

        [Fact]
        public void CreateLeatherLegsLvl2()
        {
            //Arrange
            //Base bonus
            var level = 2;
            var baseBonusHP = 20;
            var baseBonusInt = 0;
            var baseBonusDex = 3;
            var baseBonusStr = 1;
            // Scaling by armor level on cloth
            var hpScaling = 8;
            var intScaling = 0;
            var dexScaling = 2;
            var strScaling = 1;
            // Scaling by slot type (body part)
            var slotScaling = 0.6;
            // Formula for bonus stats on armor
            var bonusHP = (int)Math.Floor((baseBonusHP + hpScaling * level) * slotScaling);
            var bonusInt = (int)Math.Floor((baseBonusInt + intScaling * level) * slotScaling);
            var bonusDex = (int)Math.Floor((baseBonusDex + dexScaling * level) * slotScaling);
            var bonusStr = (int)Math.Floor((baseBonusStr + strScaling * level) * slotScaling);

            //Act
            var armor = new Leather("Legs Leather", SlotType.Legs, 2);

            //Assert
            Assert.Equal(bonusHP, armor.Bonus.Health);
            Assert.Equal(bonusInt, armor.Bonus.Intelligence);
            Assert.Equal(bonusDex, armor.Bonus.Dexterity);
            Assert.Equal(bonusStr, armor.Bonus.Strength);
        }
    }
}
