using System.Collections.Generic;
using assignment_2_rpg_characters.Builder;
using assignment_2_rpg_characters.Enums;
using assignment_2_rpg_characters.Interfaces;
using assignment_2_rpg_characters.Models.Armors;
using assignment_2_rpg_characters.Models.Heroes;
using Xunit;

namespace RPG.Tests
{
    public class SlotTests
    {
        [Fact]
        public void AddSlots()
        {
            var heroBuilder = new HeroBuilder();
            var hero = new Warrior("Myamoto Musashi", 15);
            var slots = new List<ISlot>
            {
                 new Plate("Body Plate", SlotType.Body, 15),
                 new Cloth("Legs Cloth", SlotType.Legs, 10),
                 new Leather("Body Leather", SlotType.Body, 8)
            };
            heroBuilder.AddSlots(hero, slots);

            Assert.True(hero.Slots.Count == 2, $"Count is {hero.Slots.Count} and should be 2");
        }

        public void ReplaceSlots()
        {
            var heroBuilder = new HeroBuilder();
            var hero = new Warrior("Myamoto Musashi", 15);
            var slots = new List<ISlot>
            {
                new Plate("Body Plate", SlotType.Body, 15),
                new Cloth("Legs Cloth", SlotType.Legs, 10),
                new Leather("Body Leather", SlotType.Body, 8)
            };
            heroBuilder.AddSlots(hero, slots);

            Assert.True(hero.Slots.Count == 2, $"Count is {hero.Slots.Count} and should be 2");
        }

        [Fact]
        public void ClearSlots()
        {
            var heroBuilder = new HeroBuilder();
            var hero = new Warrior("Myamoto Musashi", 15);
            var slots = new List<ISlot>
            {
                 new Plate("Body Plate", SlotType.Body, 15),
                 new Cloth("Legs Cloth", SlotType.Legs, 10),
                 new Leather("Body Leather", SlotType.Body, 8)
            };
            heroBuilder.AddSlots(hero, slots);
            heroBuilder.ClearSlots(hero);

            Assert.True(hero.Slots.Count == 0, $"Count is {hero.Slots.Count} and should be 0");
        }
    }
}
